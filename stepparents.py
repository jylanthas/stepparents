theword = 'stepparents'

# add words you can spell using letters from theword
possible_words = [
  'rent',
  'pest',
  'test',
  'tent',
  'rest',
  'steep',
  'step',
  'are',
  'tear',
  'septa',
  'rap',
  'seen',
  'parent',
  'tare',
  'stare',
  'pen',
  'ten',
  'nap',
  'teen',
  'parts',
  'present',
  'street',
  'strap',
  'past',
  'derp',
]

"""
return word which results from removing letters of word2 from word1.
word_remove('parts', 'pat') -> 'rs'
"""
def word_remove(word1, word2):
  tokens = list(word1)
  for t in list(word2):
    tokens.remove(t)
  return ''.join(tokens)

"""
look for word in words that can be spelled using letters
if match, return index, else -1
"""
def lettermatch(letters, words):
  for i,w in enumerate(words):
    if sorted(list(w)) == sorted(list(letters)):
      return i
  return -1

for i,wp in enumerate(possible_words):
  try:
    wremain = word_remove(theword, wp)
  except Exception as e:
    print(f"warn! '{wp}' cannot be spelled using letters '{theword}'")
    continue
  
  wpi = lettermatch(wremain, possible_words)
  notice = f'<< {wpi}' if wpi >= 0 else ''
  print(f"{i} {wp} {wremain} {notice}")
