# stepparents

Helper utils for solving the following puzzle:

Take the word "stepparents." Rearrange these 11 letters to spell two words that are opposites. What are they?
